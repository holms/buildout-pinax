.PHONY: all

all: develop


###############
# Development #
###############

.PHONY: develop

develop: clean bootstrap.py buildout 

bootstrap.py:
		mkdir eggs
		wget http://www.python-distribute.org/bootstrap.py
		python bootstrap.py

buildout:
	bin/buildout  -c development.cfg -N 

run:
	bin/django runserver

graph:
	 bin/django graph_models \
	 --group-models \
	 --all-applications \
	 -o graph.png

tags:
	bin/ctags -v

test:
	bin/django test

static:
	bin/django build_static --noinput

.PHONY: clean

clean:
	-rm -rf bin
	-rm -rf bootstrap.py
	-rm -rf eggs
	-rm -rf parts
	-rm -rf .installed.cfg
	-rm -rf develop-eggs
